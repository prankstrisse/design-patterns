package main.beverages;

/**
 * User: prankstrisse
 * Date: 1/20/14
 */
public class Espresso extends Beverage {

    public Espresso() {
        description = "Espresso";
    }

    @Override
    public double cost() {
        return 1.99;
    }
}
