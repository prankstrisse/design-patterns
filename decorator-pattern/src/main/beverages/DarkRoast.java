package main.beverages;

/**
 * User: prankstrisse
 * Date: 1/20/14
 */
public class DarkRoast extends Beverage {

    public DarkRoast() {
        description = "Dark Roast Coffee";
    }

    @Override
    public double cost() {
        return .99;
    }
}
