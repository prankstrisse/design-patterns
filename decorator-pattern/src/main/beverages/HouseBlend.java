package main.beverages;

/**
 * User: prankstrisse
 * Date: 1/20/14
 */
public class HouseBlend extends Beverage {
    public HouseBlend() {
        description = "House Blend Coffee";
    }

    @Override
    public double cost() {
        return .89;
    }
}
