package main.beverages;

/**
 * User: prankstrisse
 * Date: 1/20/14
 */
public abstract class Beverage {
    String description = "Unknown beverage";

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}
