package main.condiments;

import main.beverages.Beverage;

/**
 * User: prankstrisse
 * Date: 1/20/14
 */
public abstract class CondimentDecorator extends Beverage {
    public abstract String getDescription();
}
