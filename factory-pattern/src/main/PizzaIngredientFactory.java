package main;

import main.pizza.ingredients.*;

/**
 * User: prankstrisse
 * Date: 1/24/14
 */
public interface PizzaIngredientFactory {
    Dough createDough();
    Sauce createSauce();
    Cheese createCheese();
    Veggies[] createVeggies();
    Pepperoni createPepperoni();
    Clams createClams();


}
