package test;

import main.ChicagoPizzaStore;
import main.NYPizzaStore;
import main.pizza.Pizza;
import main.PizzaStore;

/**
 * User: prankstrisse
 * Date: 1/22/14
 */
public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NYPizzaStore();
        PizzaStore chicagoStore = new ChicagoPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza.getName() + "\n");

        pizza = chicagoStore.orderPizza("cheese");
        System.out.println("Joel ordered a " + pizza.getName() + "\n");


    }
}
