package main;

/**
 * User: prankstrisse
 * Date: 1/17/14
 */
public interface Observer {
    void update(float temp, float humidity, float pressure);
}
