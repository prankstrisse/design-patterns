package main;

/**
 * User: prankstrisse
 * Date: 1/17/14
 */
public interface Subject {
    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notiyObservers();
}
