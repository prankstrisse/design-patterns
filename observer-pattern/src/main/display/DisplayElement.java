package main.display;

/**
 * User: prankstrisse
 * Date: 1/17/14
 */
public interface DisplayElement {
    void display();
}
