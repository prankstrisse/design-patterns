package test;

import main.Duck;
import main.MallardDuck;
import main.ModelDuck;
import main.behavior.impl.FlyRocketPowered;

/**
 * User: prankstrisse
 * Date: 1/15/14
 */
public class MiniDuckSimulator {
    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}
