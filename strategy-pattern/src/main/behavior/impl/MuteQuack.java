package main.behavior.impl;

import main.behavior.QuackBehavior;

/**
 * User: prankstrisse
 * Date: 1/15/14
 */
public class MuteQuack implements QuackBehavior {

    @Override
    public void quack() {
        System.out.println("<< Silence >>");
    }
}
