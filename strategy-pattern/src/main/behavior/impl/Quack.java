package main.behavior.impl;

import main.behavior.QuackBehavior;

/**
 * User: prankstrisse
 * Date: 1/15/14
 */
public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("main.behavior.impl.Quack");
    }
}
