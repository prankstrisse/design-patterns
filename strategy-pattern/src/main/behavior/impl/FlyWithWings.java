package main.behavior.impl;

import main.behavior.FlyBehavior;

/**
 * User: prankstrisse
 * Date: 1/15/14
 */
public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("I'm flying!");
    }

}
