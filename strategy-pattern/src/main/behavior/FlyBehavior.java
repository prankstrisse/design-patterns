package main.behavior;

/**
 * User: prankstrisse
 * Date: 1/15/14
 */
public interface FlyBehavior {
    void fly();
}
