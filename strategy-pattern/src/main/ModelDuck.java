package main;

import main.behavior.impl.FlyNoWay;
import main.behavior.impl.Quack;

/**
 * User: prankstrisse
 * Date: 1/15/14
 */
public class ModelDuck extends Duck {

    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("I'm a model duck");
    }
}
