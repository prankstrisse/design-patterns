package main;

/**
 * User: prankstrisse
 * Date: 1/28/14
 */
public class NoCommand implements Command {
    @Override
    public void execute() {
    }

    @Override
    public void undo() {
    }
}
