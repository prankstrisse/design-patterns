package main;

/**
 * User: prankstrisse
 * Date: 1/28/14
 */
public interface Command {
    void execute();
    void undo();
}
