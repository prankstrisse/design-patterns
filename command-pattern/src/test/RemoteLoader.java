package test;

import main.*;

/**
 * User: prankstrisse
 * Date: 1/28/14
 */
public class RemoteLoader {
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();

        Light light = new Light("Living Room");
        TV tv = new TV("Living Room");
        Stereo stereo = new Stereo("Living Room");
        Hottub hottub = new Hottub();

        LightOnCommand lightOn = new LightOnCommand(light);
        StereoOnCommand stereoOn = new StereoOnCommand(stereo);
        TVOnCommand tvOn = new TVOnCommand(tv);
        HottubOnCommand hottubOn = new HottubOnCommand(hottub);


        LightOffCommand lightOff = new LightOffCommand(light);
        StereoOffCommand stereoOff = new StereoOffCommand(stereo);
        TVOffCommand tvOff = new TVOffCommand(tv);
        HottubOffCommand hottubOff = new HottubOffCommand(hottub);

        MacroCommand partyOn = new MacroCommand(new Command[] {lightOn, stereoOn, tvOn, hottubOn});
        MacroCommand partyOff = new MacroCommand(new Command[] {lightOff, stereoOff, tvOff, hottubOff});

        remoteControl.setCommand(0, partyOn, partyOff);

        System.out.println("--- Pushing Macro On ---");
        remoteControl.onButtonWasPushed(0);
        System.out.println("--- Pushing Macro Off ---");
        System.out.println(remoteControl);
        remoteControl.undoButtonWasPushed();
//        remoteControl.offButtonWasPushed(0);
    }
}
